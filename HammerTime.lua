--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if HammerTime == nil then HammerTime = {} end 

local HammerTime 	= HammerTime

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local alpha = false
--[===[@alpha@
alpha = true
--@end-alpha@]===]


local VERSION 			= { MAJOR = 1, MINOR = 1, REV = 1 }
local DisplayVersion 	= string.format( "%d.%d.%d", VERSION.MAJOR, VERSION.MINOR, VERSION.REV )

if( debug ) then
	DisplayVersion 		= DisplayVersion .. " Dev"
else
	DisplayVersion 		= DisplayVersion .. " r" .. ( tonumber( "6" ) or "0" )
	
	if( alpha ) then
		DisplayVersion = DisplayVersion .. "-alpha"
	end
end

local firstLoad = true

function HammerTime.GetVersion() return DisplayVersion end

function HammerTime.OnInitialize()
	RegisterEventHandler( SystemData.Events.RELOAD_INTERFACE, "HammerTime.OnLoad" )
	RegisterEventHandler( SystemData.Events.LOADING_END, "HammerTime.OnLoad" )
end

function HammerTime.OnLoad()
	if( firstLoad ) then
		
		HammerTime._SalvagingWindow_OnAcceptLButtonUp			= SalvagingWindow.OnAcceptLButtonUp
		SalvagingWindow.OnAcceptLButtonUp						= HammerTime.OnAcceptLButtonUp
	
		d( "HammerTime loaded." )
		
		firstLoad = false
	end
end

function HammerTime.OnAcceptLButtonUp(...)
	HammerTime._SalvagingWindow_OnAcceptLButtonUp(...)
	SalvagingWindow.BeginSalvage()
end