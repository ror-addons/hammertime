<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
	<UiMod name="HammerTime" version="1.1.0" date="2010/10">
		<Author name="Wikki" email="wikkifizzle@gmail.com" />
		<Description text="HammerTime - Salvage items just a little bit faster!" />
		<VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />
	
		<Dependencies>
			<Dependency name="EA_CraftingSystem" />
	    </Dependencies>
        
		<Files>
			<File name="HammerTime.lua" />
		</Files>

		<OnUpdate/>
		
		<OnInitialize>
			<CallFunction name="HammerTime.OnInitialize" />
		</OnInitialize>
		<OnShutdown/>
		
		<WARInfo>
		    <Categories>
		        <Category name="ITEMS_INVENTORY" />
		        <Category name="CRAFTING" />
		    </Categories>
		    <Careers>
		        <Career name="BLACKGUARD" />
		        <Career name="WITCH_ELF" />
		        <Career name="DISCIPLE" />
		        <Career name="SORCERER" />
		        <Career name="IRON_BREAKER" />
		        <Career name="SLAYER" />
		        <Career name="RUNE_PRIEST" />
		        <Career name="ENGINEER" />
		        <Career name="BLACK_ORC" />
		        <Career name="CHOPPA" />
		        <Career name="SHAMAN" />
		        <Career name="SQUIG_HERDER" />
		        <Career name="WITCH_HUNTER" />
		        <Career name="KNIGHT" />
		        <Career name="BRIGHT_WIZARD" />
		        <Career name="WARRIOR_PRIEST" />
		        <Career name="CHOSEN" />
		        <Career name= "MARAUDER" />
		        <Career name="ZEALOT" />
		        <Career name="MAGUS" />
		        <Career name="SWORDMASTER" />
		        <Career name="SHADOW_WARRIOR" />
		        <Career name="WHITE_LION" />
		        <Career name="ARCHMAGE" />
		    </Careers>
		</WARInfo>

	</UiMod>
</ModuleFile>